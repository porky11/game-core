use num_traits::real::Real;
use vector_space::InnerSpace;

pub fn timed_friction<R: Real>(strength: R, timestep: R) -> R {
    let two = R::one() + R::one();
    R::one() - two.powf(-strength * timestep)
}

pub fn directional_friction<V: InnerSpace>(
    vel: V,
    dir: V,
    parallel_friction: V::Scalar,
    orthogonal_friction: V::Scalar,
) -> V {
    let parallel_vel = vel.project(dir);
    let orthogonal_vel = vel - parallel_vel;

    parallel_vel * parallel_friction + orthogonal_vel * orthogonal_friction
}
